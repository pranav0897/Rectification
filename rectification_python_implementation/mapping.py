import cv2
import numpy as np
import matplotlib.pyplot as plt
# from sklearn.preprocessing import PolynomialFeatures
from numpy import polyfit
from collections import Counter
from collections import namedtuple
import PIL
from PIL import Image

# from lookup_table import LUT


def to_homog(points): #here always remember that points is a 3x4 matrix
   # write your code here
    if len(points.shape)==1:
       # only 1 point is passed:
        a=np.ones((len(points)+1),np.float32)
        a[0]=points[0]
        a[1]=points[1]
        return a
    r,c=(points.shape[0]+1,points.shape[1])
    a=np.ones((r,c),type(points[0,0]))
    a[:r-1,:]=points[:,:]
    return a# convert points from homogeneous to euclidian
def from_homog(points_homog):
   # write your code here
    if len(points_homog.shape)==1:
       # only 1 point is passed:
        a=np.zeros((len(points_homog)-1),np.float32)
        a[0]=points_homog[0]/points_homog[2]
        a[1]=points_homog[1]/points_homog[2]
        return a
    n=points_homog.shape[0]-1
    m=points_homog.shape[1]
    b=np.zeros((n,m),np.float32)
    for i in range(n):
        b[i,:]=points_homog[i,:]/points_homog[n,:]    
    return b# project 3D euclidian points to 2D euclidian
def project_points(P_int, P_ext, pts):
   # write your code here
   # assuming P_int is the camera matrix (3x4)
   # P_ext is 4x4
   # pts are 3x1 (non homogenous)
    print(np.shape(P_int))
    print(np.shape(P_ext))
    print(np.shape(to_homog(pts)))
    pts_final=np.matmul(P_int,np.matmul(P_ext,to_homog(pts)))
    return from_homog(pts_final)


def computeH(source_points, target_points):
    # returns the 3x3 homography matrix such that:
    # np.matmul(H, source_points) = target_points
    # where source_points and target_points are expected to be in homogeneous
    # make sure points are 3D homogeneous
    assert source_points.shape[0]==3 and target_points.shape[0]==3
    #Your code goes here
    H_mtx = np.zeros((3,3)) #Fill in the H_mtx with appropriate values.
    #source to e -> H1, e to target -> H2_inv
    # will calculate H1, and H2. Then do H1.H2^-1
    # H1
    xin=source_points.T
    print("XIN",xin)
    xinmat=np.hstack((xin[0,:].reshape(-1,1),xin[1,:].reshape(-1,1),xin[2,:].reshape(-1,1)))
    print("XINMAT",xinmat)
    lambdas1=np.matmul(np.linalg.inv(xinmat),xin[3,:].reshape(-1,1)).reshape(-1)    
    print("LAMBDAS1",lambdas1)
    H1inv=np.hstack((xin[0,:].reshape(-1,1)*lambdas1[0],
                    xin[1,:].reshape(-1,1)*lambdas1[1],
                    xin[2,:].reshape(-1,1)*lambdas1[2]))
    print("H1inv",H1inv)
    xout=target_points.T
    print("XOUT",xout)
    xoutmat=np.hstack((xout[0,:].reshape(-1,1),xout[1,:].reshape(-1,1),xout[2,:].reshape(-1,1)))
    print("XOUTMAT",xoutmat)
    lambdas2=np.matmul(np.linalg.inv(xoutmat),xout[3,:].reshape(-1,1)).reshape(-1)
    print("LAMBDAS2",lambdas2)
    H2inv=np.hstack((xout[0,:].reshape(-1,1)*lambdas2[0],
                   xout[1,:].reshape(-1,1)*lambdas2[1],
                   xout[2,:].reshape(-1,1)*lambdas2[2]))
    print("H2inv",H2inv)
    H_mtx=np.matmul(H2inv,np.linalg.inv(H1inv))
    print("***********")
    return  H_mtx


def distance(x1,y1,x2,y2,metric='l1'):
    if metric=='l1':
        return np.abs(x1-x2)+np.abs(y1-y2)
    if metric=='l2':
        return np.sqrt((x1-x2)**2+(y1-y2)**2)

def n_nearest_neighbours(x,y,n=8,dist='l2'):
    '''Takes a point in 2D space,
    returns n nearest neighbours (n<=15)
    '''
    nn=[]
    fracx=x%1.0
    fracy=y%1.0
    xfloor=np.floor(x)
    yfloor=np.floor(y)
    xceil=np.ceil(x)
    yceil=np.ceil(y)
    window_size=int(np.ceil(np.sqrt(n)))
    xns=set(range(int(xfloor)-int(window_size/2),int(xceil)+int(window_size/2)+1))
    yns=set(range(int(yfloor)-int(window_size/2),int(yceil)+int(window_size/2)+1))
#     xns=set([xfloor-2,xfloor-1,xfloor,xceil,xceil+1,xfloor+2])
#     yns=set([yfloor-1,yfloor,yceil,yceil+1])
        
    for xn in xns:
        for yn in yns:
            a=distance(x,y,xn,yn,metric='l2')
#             print(type(a))
#             print(type(xn))
#             print(type(yn))
            nn.append((a,xn,yn))
    nn=sorted(nn)
    return nn[:n]

def forward_mapping_with_nearest_nbrs(source_img, source_points, target_size):
    # Create a target image and select target points to create a homography from source image to target image,
    # in other words map all source points to target points and then create
    # a warped version of the image based on the homography by filling in the target image.
    # Make sure the new image (of size target_size) has the same number of color channels as source image
    assert target_size[2]==source_img.shape[2]
    #Your code goes here
#     target_size=(int(target_size[0]/2),int(target_size[1]/2),target_size[2])
#     target_size[1]/=2.0
    h=target_size[0]-1#nrows (max for i)
    w=target_size[1]-1#ncols (max for j)
    print("h,w:",h,w)
    target_points_xy=np.asarray([[0,h],[0.0,0.0],[w,0],[w,h]]).T
    target_img=np.zeros(target_size)
    secondary_buffer=np.zeros(target_size[:2])
    for pts in target_points_xy.T:
        print(pts,target_img[int(pts[1]),int(pts[0])])

    print("Source Points coordinates:",source_points)
    print("Target Points coordinates:",target_points_xy)
    H=computeH(to_homog(source_points),to_homog(target_points_xy))
    for pts in source_points.T:
        homogpts=to_homog(pts)
        print("Source Point coordinates:",pts)
        outpts=from_homog(np.matmul(H,homogpts))
        print("Corresponding Output point coordinates",(int(outpts[0]),int(outpts[1])))
    for xin in range(source_img.shape[1]):
        for yin in range(source_img.shape[0]):
            inpts=to_homog(np.asarray([xin,yin]))
            
            (x,y)=from_homog(np.matmul(H,inpts))
            
            
            if x<4 or x>target_img.shape[1]-4 or y<4 or y>target_img.shape[0]-4:
                continue
                
            
                
            nbrs=n_nearest_neighbours(y,x,n=1,dist='l2')
            intensity=source_img[yin,xin,:]
            for nbr in nbrs:
                if len(nbr)!=3:
                    print(nbr)
                    continue
#                 if nbr[0]==0.0:
#                     target_img[int(nbr[1]),int(nbr[2])]+=intensity    
#                     secondary_buffer[int(nbr[1]),int(nbr[2])]+=1
                if nbr[1]<0 or nbr[1]>=target_img.shape[0] or nbr[0]<0 or nbr[0]>=target_img.shape[1]:
                    print(nbr)
                    print(x,y)
                    print(xin,yin)
                else:
#                     target_img[int(nbr[1]),int(nbr[2])]+=intensity
#                     print("***************")
#                     print(target_img[int(nbr[1]),int(nbr[2])])
#                     print(intensity)
#                     print(np.exp(-1*nbr[0]))
#                     print("++++++++++++++++++++")
                    target_img[int(nbr[1]),int(nbr[2])]+=intensity*np.exp(-1*nbr[0])
                        
#                         target_img[int(nbr[1]),int(nbr[2])]+=intensity/nbr[0]
                    secondary_buffer[int(nbr[1]),int(nbr[2])]+=np.exp(-1*nbr[0])
                
    for yin in range(target_img.shape[0]):
        for xin in range(target_img.shape[1]):
            if secondary_buffer[yin,xin]!=0:
                target_img[yin,xin,:]/=secondary_buffer[yin,xin]
                print("(((((((((((((((((((((((((((((((((")
                print(target_img[yin,xin,:])
                print(secondary_buffer[yin,xin])
                print("))))))))))))))))))))))))))))))))))")
#             target_img[int(y),int(x),:]=source_img[yin,xin,:]
    return target_img


def backward_mapping(source_img, source_points, target_size):
    # Create a target image and select target points to create a homography from target image to source image,
    # in other words map each target point to a source point, and then create a warped version
    # of the image based on the homography by filling in the target image.
    # Make sure the new image (of size target_size) has the same number of color channels as source image
    h=target_size[0]#nrows
    w=target_size[1]#ncols
    target_points_xy=np.asarray([[0,h],[0.0,0.0],[w,0],[w,h]]).T
    target_img=np.zeros(target_size)
    print(source_points)
    H=computeH(to_homog(target_points_xy),to_homog(source_points))
    for pts in target_points_xy.T:
        homogpts=to_homog(pts)
        print("Source Point:",pts)
        outpts=from_homog(np.matmul(H,homogpts))
        print("Corresponding Output point",outpts)

    for xin in range(target_img.shape[1]):
        for yin in range(target_img.shape[0]):
            inpts=to_homog(np.asarray([xin,yin]))
            
            (xout,yout)=from_homog(np.matmul(H,inpts))
            
            
            if xout<1 or xout>=source_img.shape[1]-1 or yout<1 or yout>=source_img.shape[0]-1:
                continue
            
            j,i=xout,yout
            fraci=i%1.0
            fracj=j%1.0
            intensity=(1-fraci)*(1-fracj)*source_img[int(np.floor(i))][int(np.floor(j))]+\
                        (fraci)*(1-fracj)*source_img[int(np.ceil(i))][int(np.floor(j))]+\
                        (fraci)*(fracj)*source_img[int(np.ceil(i))][int(np.ceil(j))]+\
                        (1-fraci)*(fracj)*source_img[int(np.floor(i))][int(np.ceil(j))]
                        
            target_img[yin,xin]=intensity
    return target_img
    #Your code goes here

# Use the code below to plot your result



def driver_function(filename="./photo.jpg",source_points=None):
    source_image=cv2.imread("./photo.jpg")
    plt.imshow(source_image)

    x_coords = [1946,105,511,1944]
    y_coords = [1499,1159,125,217]# Plot points from the previous problem is used to draw over your image
    # Note that your coordinates will change once you resize your image again
    source_points = np.vstack((x_coords, y_coords))


    print(source_points)
    result = backward_mapping(source_image, source_points, (source_image.shape[1],source_image.shape[0],3)) #Choose appropriate size
    result/=255.0
    plt.subplot(1, 2, 1)
    plt.imshow(source_image)
    plt.subplot(1, 2, 2)
    plt.imshow(result)
    plt.imsave("backward_mapping.png",result)
    plt.show()

    # Use the code below to plot your result
    # source_points=np.asarray([[105.0,115.0],[50.0,10.0],[195.0,19.0],[195.0,150.0]]).T.astype(np.float32)
    # print("Source Points:",source_points)
    # print(source_image.shape)
    result = forward_mapping_with_nearest_nbrs(source_image, source_points, (source_image.shape[1],source_image.shape[0],3)) #Choose appropriate target size
    result/=255.0
    plt.subplot(1, 2, 1)
    plt.imshow(source_image)
    plt.subplot(1, 2, 2)
    plt.imsave("fwd_mapping_1nn_exp_decay_weigh_by_exp.png",result)
    plt.imshow(result)
    plt.show()

def get_fwd_mapping_H():
    '''
    Computes the H matrix (3x3) using the image of a piece of paper on a table, 
    converting it to a scanned-like image of the paper, except it is rotated 90 degrees (for minimal distortion)
    Returns the H matrix (3x3) needed for LUT generation
    '''
    source_image=cv2.imread("./photo.jpg")
    # plt.imshow(source_image)

    x_coords = [1946,105,511,1944]
    y_coords = [1499,1159,125,217]# Plot points from the previous problem is used to draw over your image
    # Note that your coordinates will change once you resize your image again
    source_points = np.vstack((x_coords, y_coords))

    h=source_image.shape[0]-1 #nrows (max for i)
    w=source_image.shape[1]-1 #ncols (max for j)

    target_size=(h+1,w+1,3)
    #nrows (max for i)
    #ncols (max for j)
    print("h,w:",h,w)
    target_points_xy=np.asarray([[w,h],[0,h],[0.0,0.0],[w,0]]).T
    # target_points_xy=np.asarray([[0.0,0.0],[w,0],[w,h],[0,h]]).T
    target_img=np.zeros(target_size)
    secondary_buffer=np.zeros(target_size[:2])
    for pts in target_points_xy.T:
        print(pts,target_img[int(pts[1]),int(pts[0])])

    print("Source Points coordinates:",source_points)
    print("Target Points coordinates:",target_points_xy)
    H=computeH(to_homog(source_points),to_homog(target_points_xy))
    for pts in source_points.T:
        homogpts=to_homog(pts)
        print("Source Point coordinates:",pts)
        outpts=from_homog(np.matmul(H,homogpts))
        print("Corresponding Output point coordinates",(int(outpts[0]),int(outpts[1])))
    return H

def get_bwd_mapping_H():
    source_image=cv2.imread("./photo.jpg")
    # plt.imshow(source_image)

    x_coords = [1946,105,511,1944]
    y_coords = [1499,1159,125,217]# Plot points from the previous problem is used to draw over your image
    # Note that your coordinates will change once you resize your image again
    source_points = np.vstack((x_coords, y_coords))

    #change this if going for portrait mode
    h=source_image.shape[0]-1 #nrows (max for i)
    W=source_image.shape[1]-1 #ncols (max for j)

    target_size=(h+1,w+1,3)

    # h=target_size[0]#nrows
 #    w=target_size[1]#ncols
    #change this if going for portrait mode
    target_points_xy=np.asarray([[w,h],[0,h],[0.0,0.0],[w,0]]).T
    # target_points_xy=np.asarray([[0,h],[0.0,0.0],[w,0],[w,h]]).T
    target_img=np.zeros(target_size)
    print(source_points)
    H=computeH(to_homog(target_points_xy),to_homog(source_points))

    return H