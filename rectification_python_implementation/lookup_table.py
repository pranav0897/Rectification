import cv2
import numpy as np
import matplotlib.pyplot as plt
# from sklearn.preprocessing import PolynomialFeatures
from numpy import polyfit
from collections import Counter
from collections import namedtuple
import PIL
from PIL import Image

class LUT:

    def to_homog(self,points): #here always remember that points is a 3x4 matrix
       # write your code here
        if len(points.shape)==1:
           # only 1 point is passed:
            a=np.ones((len(points)+1),np.float32)
            a[0]=points[0]
            a[1]=points[1]
            return a
        r,c=(points.shape[0]+1,points.shape[1])
        a=np.ones((r,c),type(points[0,0]))
        a[:r-1,:]=points[:,:]
        return a# convert points from homogeneous to euclidian
    def from_homog(self,points_homog):
       # write your code here
        if len(points_homog.shape)==1:
           # only 1 point is passed:
            a=np.zeros((len(points_homog)-1),np.float32)
            a[0]=points_homog[0]/points_homog[2]
            a[1]=points_homog[1]/points_homog[2]
            return a
        n=points_homog.shape[0]-1
        m=points_homog.shape[1]
        b=np.zeros((n,m),np.float32)
        for i in range(n):
            b[i,:]=points_homog[i,:]/points_homog[n,:]    
        return b# project 3D euclidian points to 2D euclidian

    # def get_xy_coords(self,mat,x,y):
    #     '''Wrapper to get xy coordinates from perspective transformation matrix
    #     '''
    #     denominator=mat[2,0]*x + mat[2,1]*y + mat[2,2]
    #     xo=(mat[0,0]*x + mat[0,1]*y + mat[0,2])/denominator
    #     yo=(mat[1,0]*x + mat[1,1]*y + mat[1,2])/denominator
    #     return xo, yo
    def __init__(self,proj_xform_mat,img_size):
        '''Takes the Projective Transform Matrix and the size of reference image,
        and creates forward and backward lookup tables for it.
        '''
        assert (len(img_size)>=2)
        assert (len(proj_xform_mat.shape)==2 and proj_xform_mat.shape[0]>=3 and proj_xform_mat.shape[1]>=3)
        self.LUT_fwd_x=np.zeros((img_size[:2]))
        self.LUT_fwd_y=np.zeros((img_size[:2]))
#         print(LUT_fwd_x.shape,LUT_fwd_y.shape)
        self.xrmin=1e6
        self.xrmax=-1e6
        self.yrmin=1e6
        self.yrmax=-1e6
        for xin in range(img_size[1]):
            for yin in range(img_size[0]):
                x,y=self.from_homog(np.matmul(proj_xform_mat,self.to_homog(np.asarray([xin,yin]))))
                self.LUT_fwd_x[yin,xin]=x
                self.LUT_fwd_y[yin,xin]=y
                self.xrmin=min(self.xrmin,x)
                self.xrmax=max(self.xrmax,x)
                self.yrmin=min(self.yrmin,y)
                self.yrmax=max(self.yrmax,y)
        
#         self.xrmin=int(np.floor(self.xrmin))
#         self.xrmax=int(np.floor(self.xrmax))
#         self.yrmin=int(np.floor(self.yrmin))
#         self.yrmax=int(np.floor(self.yrmax))
        print(self.xrmin,self.xrmax,self.yrmin,self.yrmax)
        # computing the size of the undistorted matrix
        self.fwd_map_range_x=int(np.ceil(self.xrmax-self.xrmin)+1)
        self.fwd_map_range_y=int(np.ceil(self.yrmax-self.yrmin)+1)
        
        self.inv_proj_xform_mat=np.linalg.inv(proj_xform_mat)
        # defining LUTs for backward mapping
        #####################################
        # assuming that we feed in i,j and get back x,y.
        #####################################
        self.LUT_bwd_x=np.zeros((self.fwd_map_range_y,self.fwd_map_range_x))
        self.LUT_bwd_y=np.zeros((self.fwd_map_range_y,self.fwd_map_range_x))
                
        for xin in range(self.fwd_map_range_x):
            for yin in range(self.fwd_map_range_y):
                xu=xin+self.xrmin
                yu=yin+self.yrmin
                xd,yd=self.from_homog(np.matmul(self.inv_proj_xform_mat,self.to_homog(np.asarray([xu,yu]))))
                self.LUT_bwd_x[yin,xin]=xd
                self.LUT_bwd_y[yin,xin]=yd
        
        self.poly_degree=3

        self.small_LUT_fwd_x, self.small_LUT_fwd_y,\
        self.small_LUT_bwd_x, self.small_LUT_bwd_y=\
        self.compress_LUT(poly_degree=self.poly_degree)     
        # self.compress_LUT2(poly_degree=self.poly_degree)

                
    def LUT_fwdf(self,i_in=None,j_in=None):
        '''Wrapper for the forward-mapping LUT
        '''
        if i_in is None and j_in is None:
            return self.LUT_fwd_x,self.LUT_fwd_y
        elif i_in is None:
            return self.LUT_fwd_x[:,j_in],self.LUT_fwd_y[:,j_in]
        elif j_in is None:
            return self.LUT_fwd_x[i_in,:],self.LUT_fwd_y[i_in,:]
        else:
            return self.LUT_fwd_x[i_in,j_in],self.LUT_fwd_y[i_in,j_in]
    def LUT_bwdf(self,iu,ju):
        '''Wrapper for the backward-mapping LUT. 
        Assumption here is that the coordinates passed to it came from the FWD LUT, and lie between 
        [self.xrmin,self.xrmax] and [self.yrmin,self.yrmax]
        '''
        
        if iu is None and ju is None:
            return self.LUT_bwd_x,self.LUT_bwd_y
        elif iu is None:
            ju1=int(np.floor(ju-self.xrmin))
            return self.LUT_bwd_x[:,ju1],self.LUT_bwd_y[:,ju1]
        elif ju is None:
            iu1=int(np.floor(iu-self.yrmin))
            return self.LUT_bwd_x[iu1,:],self.LUT_bwd_y[iu1,:]
        else:
            iu1=int(np.floor(iu-self.yrmin))
            ju1=int(np.floor(ju-self.xrmin))
            return self.LUT_bwd_x[iu1,ju1],self.LUT_bwd_y[xu1,ju1]
    def compress_LUT2(self,poly_degree=3):
        '''Original LUT was of size maxy X maxx
        We use polynomial approximation to compress this table.
        Yf=Poly_y(Yi)
        Poly_y is an n degree polynomial. whose coeffs are stored in fwd_coeffs_for_poly_y (coeffs vary with Xin)

        LUTy will compress to fwd_coeffs_for_poly_y, and become a table of size  maxx X (n+1)
 
        Xf=Poly_x(Xi)
        Poly_x is a polynomial of order n, whose coeffs are stored in fwd_coeffs_for_poly_x (coeffs vary with Yin)
        LUTx will compress to fwd_coeffs_for_poly_x, and become a table of size maxy X (n+1)
        '''
        fwd_maxy,fwd_maxx=self.LUT_fwd_x.shape[:2]
        self.fwd_coeffs_for_poly_x=np.zeros((fwd_maxy,poly_degree+1)) # takes in Yin, gives out coeffs for polynomial. Coeffs x Xin = Xout
        self.fwd_coeffs_for_poly_y=np.zeros((fwd_maxx,poly_degree+1)) # takes in Xin, gives out coeffs for polynomial. Coeffs x Yin = Yout

        for xin in range(fwd_maxx):
            yinp=range(fwd_maxy)
            _,youtp=self.LUT_fwdf(None,xin)

            xcoeffs=polyfit(yinp,youtp,deg=poly_degree)
            self.fwd_coeffs_for_poly_y[xin,:]=xcoeffs

        for yin in range(fwd_maxy):
            xinp=range(fwd_maxx)
            xoutp,_=self.LUT_fwdf(yin,None)

            ycoeffs=polyfit(xinp,xoutp,deg=poly_degree)
            self.fwd_coeffs_for_poly_x[yin,:]=ycoeffs

        bwd_maxy,bwd_maxx=self.LUT_bwd_x.shape[:2]
        self.bwd_coeffs_for_poly_x=np.zeros((bwd_maxy,poly_degree+1))
        self.bwd_coeffs_for_poly_y=np.zeros((bwd_maxx,poly_degree+1))

        for xin in range(bwd_maxx):
            yinp=range(bwd_maxy)
            _,youtp=self.LUT_bwdf(None,xin)

            xcoeffs=polyfit(yinp,youtp,deg=poly_degree)
            self.bwd_coeffs_for_poly_y[xin,:]=xcoeffs
        
        for yin in range(bwd_maxy):
            xinp=range(bwd_maxx)
            xoutp,_=self.LUT_bwdf(yin,None)

            ycoeffs=polyfit(xinp,xoutp,deg=poly_degree)
            self.bwd_coeffs_for_poly_x[yin,:]=ycoeffs        

        # return fwd_coeffs_for_poly_x,fwd_coeffs_for_poly_y,bwd_coeffs_for_poly_x,bwd_coeffs_for_poly_y

    def fwdf3(self, i,j):
        # assume i, j are lists

        ilist=np.asarray([i]).ravel()
        jlist=np.asarray([j]).ravel()

        xvals=[]
        yvals=[]
        for yin in ilist:
            for xin in jlist:
                ycoeffs=self.fwd_coeffs_for_poly_y[int(np.floor(xin))]
                xcoeffs=self.fwd_coeffs_for_poly_x[int(np.floor(yin))]

                yvar=[yin**p for p in range(self.poly_degree,-1,-1)]
                xvar=[xin**p for p in range(self.poly_degree,-1,-1)]
                xval=np.dot(yvar,ycoeffs)
                yval=np.dot(xvar,xcoeffs)
                xvals.append(xval)
                yvals.append(yval)
                    
        xvals=np.asarray(xvals)
        yvals=np.asarray(yvals)
        if len(xd2)==1 and len(yd2)==1:
            return xvals[0],yvals[0]
        else:
            return xvals,yvals

    def compress_LUT(self,poly_degree=3):
        '''Original LUT was of size maxy X maxx
        We use polynomial approximation to compress this table.
        Compress_along_axis = 0
        Yf=Pn(Yi)
        Pn = F(Xi)
        Where Pn is a polynomial of degree n
        LUT will become a table of size  maxx X (n+1)
        Compress_along_axis = 1
        Xf=Pn(Xi)
        Pn = F(Yi)
        Where Pn is a polynomial of degree n
        LUT will become a table of size maxy X (n+1)
        '''
        maxy,maxx=self.LUT_fwd_x.shape[:2]
        compressed_LUT_yout_fwd=np.zeros((maxy,poly_degree+1))
        compressed_LUT_xout_fwd=np.zeros((maxx,poly_degree+1))
        for idx in range(maxy):
            _,yu=self.LUT_fwdf(idx,None)
            x=range(maxx)
            values,residuals, rank, singular_values, rcond=polyfit(x,yu,deg=poly_degree,full=True)
            compressed_LUT_yout_fwd[idx,:]=values
            
        for idx in range(maxx):
            xu,_=self.LUT_fwdf(None,idx)
            y=range(maxy)
            values,residuals, rank, singular_values, rcond=polyfit(y,xu,deg=poly_degree,full=True)
            compressed_LUT_xout_fwd[idx,:]=values
        
        maxy,maxx=self.LUT_bwd_x.shape[:2]
        LUT_bwd2_y=np.zeros((maxy,poly_degree+1))
        LUT_bwd2_x=np.zeros((maxx,poly_degree+1))
        for idx in range(maxy):
            _,yu=self.LUT_bwdf(idx+self.yrmin,None)
            x=range(maxx)+self.xrmin
            values,residuals, rank, singular_values, rcond=polyfit(x,yu,deg=poly_degree,full=True)
            LUT_bwd2_y[idx,:]=values
        for idx in range(maxx):
            xu,_=self.LUT_bwdf(None,idx+self.xrmin)
            y=range(maxy)+self.yrmin
            values,residuals, rank, singular_values, rcond=polyfit(y,xu,deg=poly_degree,full=True)
            LUT_bwd2_x[idx,:]=values
        
        return compressed_LUT_xout_fwd, compressed_LUT_yout_fwd, LUT_bwd2_x, LUT_bwd2_y
        
    def LUT_fwdf2(self,i_in=None,j_in=None):
        '''Wrapper for the compressed forward-mapping LUT
        '''
        if i_in is None and j_in is None:
            return self.LUT_fwd_x,self.LUT_fwd_y
        elif i_in is None:
            return self.LUT_fwd_x[:,j_in],self.LUT_fwd_y[:,j_in]
        elif j_in is None:
            return self.LUT_fwd_x[i_in,:],self.LUT_fwd_y[i_in,:]
        else:
            xvals=[]
            yvals=[]
            i_in2=np.asarray(i_in).ravel()
            j_in2=np.asarray(j_in).ravel()
            #i_in, yd are arrays
            for yd1 in i_in2:
                for xd1 in j_in2:
                    xcoeffs=self.small_LUT_fwd_x[int(np.floor(xd1))]
                    ycoeffs=self.small_LUT_fwd_y[int(np.floor(yd1))]
                    yvar=[yd1**p for p in range(self.poly_degree,-1,-1)]
                    xvar=[xd1**p for p in range(self.poly_degree,-1,-1)]
                    xval=np.dot(yvar,xcoeffs)
                    yval=np.dot(xvar,ycoeffs)
                    xvals.append(xval)
                    yvals.append(yval)
            xvals=np.asarray(xvals)
            yvals=np.asarray(yvals)
            if len(i_in2)==1 and len(j_in2)==1:
                return xvals[0],yvals[0]
            else:
                return xvals,yvals
    def LUT_bwdf2(self,xu,yu):
        '''Wrapper for the compressed backward-mapping LUT. 
        Assumption here is that the coordinates passed to it came from the FWD LUT, and lie between 
        [self.xrmin,self.xrmax] and [self.yrmin,self.yrmax]
        '''
        
        if xu is None and yu is None:
            return self.LUT_bwd_x,self.LUT_bwd_y
        elif xu is None:
            yu1=int(np.floor(yu-self.yrmin))
            return self.LUT_bwd_x[:,yu1],self.LUT_bwd_y[:,yu1]
        elif yu is None:
            xu1=int(np.floor(xu-self.xrmin))
            return self.LUT_bwd_x[xu1,:],self.LUT_bwd_y[xu,:]
        else:
            xvals=[]
            yvals=[]
            xu0=np.asarray(xu).ravel()
            yu0=np.asarray(yu).ravel()
#             print(LUT_bwd_x.shape)
            #xu, yu are arrays
            for xu2 in xu0:
                for yu2 in yu0:
                    xu1=int(np.floor(xu2-self.xrmin))
                    yu1=int(np.floor(yu2-self.yrmin))
                    xcoeffs=self.small_LUT_bwd_x[xu1]
                    ycoeffs=self.small_LUT_bwd_y[yu1]
#                     print(xu1,yu1,self.small_LUT_bwd_x.shape,self.small_LUT_bwd_y.shape,self.xrmin,self.xrmax,self.yrmin,self.yrmax)
                    yvar=[yu2**p for p in range(self.poly_degree,-1,-1)]
                    xvar=[xu2**p for p in range(self.poly_degree,-1,-1)]
                    xval=np.dot(yvar,xcoeffs)
                    yval=np.dot(xvar,ycoeffs)
                    xvals.append(xval)
                    yvals.append(yval)
            xvals=np.asarray(xvals)
            yvals=np.asarray(yvals)
            if len(xu0)==1 and len(yu0)==1:
                return xvals[0],yvals[0]
            else:
                return xvals,yvals
            
        
        
#         maxx,maxy=LUT_.shape[:2]
#         residual_mean=0
#         if compress_along_axis==0:
#             LUT_compressed=np.zeros((maxx,poly_degree+1))
#             x=range(maxy)
#             for idx in range(maxx):
#                 polyvals=LUT_[idx,:]
#                 values,residuals, rank, singular_values, rcond=polyfit(x,polyvals,deg=poly_degree,full=True)
#                 LUT_compressed[idx,:]=values
#                 residual_mean+=residuals
#             return LUT_compressed
#         elif compressed_along_axis==1:
#             LUT_compressed=np.zeros((maxy,poly_degree+1))
#             x=range(maxx)
#             for idx in range(maxy):
#                 polyvals=LUT_[:,idx]
#                 values,residuals, rank, singular_values, rcond=polyfit(x,polyvals,deg=poly_degree,full=True)
#                 LUT_compressed[idx,:]=values
#                 residual_mean+=residuals
#             return LUT_compressed,residual_mean            
#                 print(values)
#                 LUT_fwd2_y.append(values)
#                 print(residuals)

if __name__ == "__main__":

    h=np.asarray([[ 1.46141531e+00,  5.73824580e-01, -8.18511296e+02],
       [-1.08373212e-01,  1.68803058e+00, -1.55625111e+02],
       [ 7.67868709e-05,  2.68210193e-04,  7.92524711e-01]])
    source_image=cv2.imread("./photo.jpg")
    tables=LUT(h,source_image.shape)