import numpy as np
import time

op_buffer_width=7
safe_interpolation_buffer_margin=3
output_stream=[]
# output_buffer_rows=[]

minx=0
maxx=op_buffer_width-safe_interpolation_buffer_margin
output_buffer_offset=1

source_image=np.zeros((2148,1637,3))
output_buffer=np.random.rand(op_buffer_width,source_image.shape[1],3)
secondary_buffer=np.random.rand(op_buffer_width,source_image.shape[1])
input_buffer=np.random.rand(source_image.shape[0],source_image.shape[1],source_image.shape[2])
output_image2=np.random.rand(source_image.shape[0],source_image.shape[1],source_image.shape[2])


init_time=time.time()
for i in range(source_image.shape[1]):
	for j in range(source_image.shape[2]):
		if not np.any(input_buffer[i,j,:]):
			continue

	# if secondary_buffer[minx%op_buffer_width,i] !=0:
		# output_image2[minx,i,:]/=secondary_buffer[minx%op_buffer_width,i]
		# output_buffer[minx%op_buffer_width,i,:]/=secondary_buffer[minx%op_buffer_width,i]		
total_time=time.time() - init_time

print("Total time for numpy any based: ",total_time)

init_time=time.time()

for i in range(source_image.shape[1]):
	for j in range(source_image.shape[2]):
		if not np.all(input_buffer[i,j,:]==0):
			continue


# output_image2[minx,:,0]=np.nan_to_num(np.divide(output_image2[minx,:,0],secondary_buffer[minx%op_buffer_width,:]))
# output_buffer[minx%op_buffer_width,:,0]=np.nan_to_num(np.divide(output_buffer[minx%op_buffer_width,:,0],secondary_buffer[minx%op_buffer_width,:]))
# output_image2[minx,:,1]=np.nan_to_num(np.divide(output_image2[minx,:,1],secondary_buffer[minx%op_buffer_width,:]))
# output_buffer[minx%op_buffer_width,:,1]=np.nan_to_num(np.divide(output_buffer[minx%op_buffer_width,:,1],secondary_buffer[minx%op_buffer_width,:]))
# output_image2[minx,:,2]=np.nan_to_num(np.divide(output_image2[minx,:,2],secondary_buffer[minx%op_buffer_width,:]))
# output_buffer[minx%op_buffer_width,:,2]=np.nan_to_num(np.divide(output_buffer[minx%op_buffer_width,:,2],secondary_buffer[minx%op_buffer_width,:]))

# total_time=time.time() - init_time

print("Total time for numpy all based: ",total_time)


init_time=time.time()

for i in range(source_image.shape[1]):
	for j in range(source_image.shape[2]):
		if input_buffer[i,j,0]==0 and input_buffer[i,j,1]==0 and input_buffer[i,j,2]==0:
			continue

print("Total time for simple comparison based: ",total_time)
